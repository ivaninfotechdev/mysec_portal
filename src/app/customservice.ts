
import { Injectable,NgZone } from '@angular/core';
import { LoadingController, ToastController, Platform, ModalController, App, ViewController, ActionSheetController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import { HTTP } from '@ionic-native/http';
declare let google: any;

@Injectable()
export class CustomService {
	
	API_BASE_URL:any = "https://dev5.ivantechnology.in/mysecportal/api_user/";
	loading:any;

	EMAIL_VALID_CHECK:any = /^.+@.+\..+$/;
	VALID_MOBILE_CHECK = /^(\+\d{1,3}[- ]?)?\d{10}$/;

	validation:any={
		checkAtoZ:/[^a-zA-Z]/gi,
		checkAtoZSpace:/[^a-zA-Z ]/gi,
		checkNumbers:/[^0-9]/gi,
		alphanumeric:/[^a-zA-Z0-9]/gi,
		alphanumericSpace:/[^a-zA-Z0-9 ]/gi,
		alphanumericWithDot:/[^a-zA-Z0-9.]/gi,
	}


	constructor(
		public actionSheetCtrl: ActionSheetController,
		public appCtrl:App,
		public modalCtrl: ModalController,
		public platform:Platform,
		public splashScreen: SplashScreen,
		public alertCtrl:AlertController, 
		public loadingCtrl:LoadingController, 
		public toastCtrl: ToastController,

		public zone: NgZone,
	) {}

	validateEmailorPhone(emailORphone) { //Validates the email or phone number together
		var phoneRegex = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5})$|^\d{10}$/; 
		return phoneRegex.test(emailORphone);
	}

	returnDate(){
		let d = new Date();
		let mm = d.getMonth()+1;
		let dd = d.getDate()+1;

		let cMonth = mm.toString().length < 2 ? '0'+ mm : mm;
		let cYear = d.getFullYear();

		let cDate = dd.toString().length < 2 ? '0'+ dd : dd;

		return cYear +'-'+ mm +'-'+dd;

	}

	returnTime(){
		let d = new Date();
		return d.getHours() + '-' + d.getMinutes()
	}
	
	keyboardHide(inputValue:any){
		// this.keyboard.hide()
	}

	presentLoadingDefault() {
		this.loading = this.loadingCtrl.create({
			content: ''
		});
		this.loading.present();
	}
	presentLoadingClose() {
		this.loading.dismiss();
	}

	presentToast(txt:any) {
		let toast = this.toastCtrl.create({
			message: txt,
			duration: 3000,
			position: 'bottom'
		});
		toast.present();
	}

	
	presentConfirm(alertTitle:any, alertMsg:any, button1:any, button2:any):Promise<any> {
		return new Promise((success)=>{
			let alert = this.alertCtrl.create({
			title: alertTitle,
			message: alertMsg,
			buttons: [
				{
					text: button1,
					role: 'Cancelar',
					handler: () => {
							success(1);
					}
				},
				{
					text: button2,
					handler: () => {
							success(2);
					}
				}
			]
			});
			alert.present();
		});
	}

	errorresponse(responseobject:any){
		this.presentToast(responseobject.message);
	}
}

