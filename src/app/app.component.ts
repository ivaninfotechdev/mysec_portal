import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  //rootPage:any
   rootPage:any = 'UserLoginPage';
  //rootPage:any = "UserOtpVerificationPage"
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleLightContent();
      statusBar.overlaysWebView(false);
      statusBar.backgroundColorByHexString('#00CD87');
      splashScreen.hide();
      // if(localStorage.getItem('userId')){
      //   splashScreen.hide();
      //   this.rootPage = 'UserDashboardPage'
      // }else{
      //   splashScreen.hide();
      //   this.rootPage = 'UserLoginPage'
      // }
    });
  }
}

