import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CustomService } from './../../app/customservice';
import { HTTP } from '@ionic-native/http';
@Injectable()
export class UserServiceProvider {

  constructor(private http: HTTP,public cs: CustomService,) {
    console.log('Hello UserServiceProvider Provider');
  }

  registrationService(urlParam,param){
   // let HEADERS: any = { 'Content-type': 'text/json; charset=utf-8' };
   //console.log(this.cs.API_BASE_URL+urlParam);
   console.log(param)
    return this.http.post(this.cs.API_BASE_URL+urlParam, param, {})
  }

  verifyOTPService(urlParam,param){
    // let HEADERS: any = { 'Content-type': 'text/json; charset=utf-8' };
    console.log(this.cs.API_BASE_URL+urlParam);
    console.log(param)
     return this.http.post(this.cs.API_BASE_URL+urlParam, param, {})
   }
   userService(urlParam,param){
    // let HEADERS: any = { 'Content-type': 'application/json; charset=utf-8' };
    console.log(this.cs.API_BASE_URL+urlParam);
    console.log(param)
     return this.http.post(this.cs.API_BASE_URL+urlParam, param, {})
   }

   visitorUserService(urlParam,param){
    // let HEADERS: any = { 'Content-type': 'application/json; charset=utf-8' };
   console.log(this.cs.API_BASE_URL+urlParam);
   console.log(param)
    // return this.http.post(this.cs.API_BASE_URL+urlParam, param, HEADERS)
    return this.http.post(this.cs.API_BASE_URL+urlParam, param, {})
  }
}
