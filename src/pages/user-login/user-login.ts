import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CustomService } from './../../app/customservice';
import { UserServiceProvider } from '../../providers/user-service/user-service';

@IonicPage()
@Component({
  selector: 'page-user-login',
  templateUrl: 'user-login.html',
})
export class UserLoginPage {
  emailORphone: any = "";
  password: any = "";

  // emailORphone: any = "sm@yopmail.com";
  // password: any = "12345678";

  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  public showPassword: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public cs: CustomService,
    public userService: UserServiceProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserLoginPage');
    localStorage.removeItem('userId')
  }


  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  signin() {
    if (!this.emailORphone.trim()){
      this.cs.presentToast('Enter email or phone number');
    }else if(!this.cs.validateEmailorPhone(this.emailORphone.toLowerCase())) {
      this.cs.presentToast('Enter valid email or phone number');
    } else if (!this.password.trim()) {
      this.cs.presentToast('Enter valid password.');
    } else {
      this.cs.presentLoadingDefault();
      let dataparams = {
        userEmailNumber: this.emailORphone.trim(),
        password: this.password.trim()
      }
      this.userService.registrationService('login', dataparams)
        .then(data => {
          console.log(data)
          let dataobj: any = JSON.parse(data.data);
          console.log(dataobj)
          if (dataobj && dataobj.personalDetails != null || dataobj.apartmentDetails !=null) {
            this.cs.presentToast(dataobj.msg);
            this.cs.presentLoadingClose();
            localStorage.setItem('userId',dataobj.personalDetails.id)
            this.navCtrl.setRoot('UserDashboardPage');
          }else{
            console.log('other err')
          }
        })
        .catch(error => {
          console.log('error', error);
          let dataobj: any = JSON.parse(error.error);
          this.cs.presentToast(dataobj.message);
          this.cs.presentLoadingClose();
        });
      
    }
  }

}
