import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CustomService } from './../../app/customservice';
import { UserServiceProvider } from '../../providers/user-service/user-service';

@IonicPage()
@Component({
  selector: 'page-add-new-visitor',
  templateUrl: 'add-new-visitor.html',
})
export class AddNewVisitorPage {
  //scheduleDate: any = '2020-06-23'


  public addVisitorParams: any = {
    fName: "",
    lName: "",
    number: "",
    category: "0",
    code: "",
    status: true,
  }

  public categoryList = [
    { label: 'VISITOR', 'value': 'V' },
    { label: 'MAID', 'value': 'm' },
    { label: 'RELATIVE', 'value': 'r' },
    { label: 'PERSONAL', 'value': 'p' },
    { label: 'DELIVERY', 'value': 'd' }
  ]
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public cs: CustomService,
    public userService: UserServiceProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddNewVisitorPage');
  }

  isvalidphoneFormat(phone) {
    var re = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[6789]\d{9}$/;
    return re.test(phone);
  }

  update() {

    if (!this.addVisitorParams.fName.trim()) {
      this.cs.presentToast('Enter visitor first name');
    } else if (!this.addVisitorParams.lName.trim()) {
      this.cs.presentToast('Enter visitor last name');
    } else if (!this.addVisitorParams.number.trim()) {
      this.cs.presentToast('Enter visitor contact number');
    } else if (this.addVisitorParams.number.length < 10) {
      this.cs.presentToast("Mobile number will atleast 10 digit");
    }
    // else if (!this.isvalidphoneFormat(this.addVisitorParams.number)) {
    // 	this.cs.presentToast("Mobile number should start with 6, 7, 8 or 9")
    // } 
    else if (this.addVisitorParams.category == '0') {
      this.cs.presentToast('Please slect visitor visit category');
    } else if (!this.addVisitorParams.code.trim()) {
      this.cs.presentToast('Enter vistor visit code');
    } else if (this.addVisitorParams.code.length < 4) {
      this.cs.presentToast('Visitor visit code should be 4 digit');
    } else if (!this.addVisitorParams.status) {
      this.cs.presentToast('Choose visit status');
    } else {
      this.cs.presentLoadingDefault();
      let UserId = localStorage.getItem('userId');
      let dataparams = {
        "fName": this.addVisitorParams.fName.toUpperCase(),
        // "fName": "DEMO",
        "number": this.addVisitorParams.number,
        "category": this.addVisitorParams.category,
        "code": this.addVisitorParams.code,
        "status": this.addVisitorParams.status == true ? "A" : "I",
        "lName": this.addVisitorParams.lName.toUpperCase(),
        // "lName": "DEMO",
        "appartmentOwnerId": UserId
      }
      console.log('get data from form', this.addVisitorParams);

      this.userService.visitorUserService('addvisitor', dataparams)
        .then(data => {
          console.log("API response : ", data)
          this.cs.presentLoadingClose();
          let dataobj: any = JSON.parse(data.data);
          console.log(dataobj)
          if (dataobj) {
            console.log(dataobj)
            this.cs.presentToast(dataobj.message);
            this.navCtrl.pop()
          }
        }).catch((err) => {
          this.cs.presentLoadingClose();
          let data = err;
          console.log("API ERROR response : ", err)
          if (data.status == 400) {
            let dataobj: any = JSON.parse(data.error);
            this.cs.presentToast(dataobj.message);
            console.log(dataobj)
          } else {
            let dataobj: any = JSON.parse(data.error);
            this.cs.presentToast(dataobj.message);
          }
        })

    }
  }
  selectValChange() {
    console.log(this.addVisitorParams.category)
  }
}
