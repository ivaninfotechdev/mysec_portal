import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddNewVisitorPage } from './add-new-visitor';

@NgModule({
  declarations: [
    AddNewVisitorPage,
  ],
  imports: [
    IonicPageModule.forChild(AddNewVisitorPage),
  ],
})
export class AddNewVisitorPageModule {}
