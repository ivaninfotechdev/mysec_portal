import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the UserDashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-dashboard',
  templateUrl: 'user-dashboard.html',
})
export class UserDashboardPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserDashboardPage');
  }
  updateAccountDetails(){
    this.navCtrl.push('UserUpdateProfilePage');
  }
  managePreferences(){
    this.navCtrl.push('ManagePreferencePage');
  }
  manageVisitor(){
    this.navCtrl.push('ManageVisitorPage');
  }
}
