import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CustomService } from './../../app/customservice';
import {UserServiceProvider} from '../../providers/user-service/user-service';
@IonicPage()
@Component({
  selector: 'page-user-registration',
  templateUrl: 'user-registration.html',
})
export class UserRegistrationPage {
  public userType = '0'
  registrationfields: any = {
    fName: "",
    lName: "",
    number: "",
    email: "",
    password: "",
    confirmPassword: "",
  }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public cs: CustomService,
    public userService:UserServiceProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserRegistrationPage');
  }

  checkValidation(value, checktype) {
    this.registrationfields[value] = this.registrationfields[value].replace(this.cs.validation[checktype], '');
  }

  registration() {
    //this.navCtrl.push('UserOtpVerificationPage')
    if(!this.registrationfields.fName.trim()) {
      this.cs.presentToast('Enter valid first name.');
    } else if(!this.registrationfields.lName.trim()) {
      this.cs.presentToast('Enter valid last name.');
    } else if(!this.registrationfields.email.trim()) {
      this.cs.presentToast('Enter Email.');
    } else if(!this.cs.EMAIL_VALID_CHECK.test(this.registrationfields.email.trim())) {
      this.cs.presentToast('Enter valid Email.');
    } else if(!this.cs.VALID_MOBILE_CHECK.test(this.registrationfields.number.trim())) {
      this.cs.presentToast('Enter valid contact number.');
    } else if (!this.registrationfields.password.trim()) {
      this.cs.presentToast('Enter password.');
    } else if(this.registrationfields.password.trim().length < 6) {
      this.cs.presentToast('Password field must be at least 6 characters in length.');
    } else if (!this.registrationfields.confirmPassword.trim() ) {
      this.cs.presentToast('Enter confirm password.');
    } else if (this.registrationfields.password.trim() != this.registrationfields.confirmPassword.trim()) {
      this.cs.presentToast('Password and confirm password must be same.');
    } else {
     this.cs.presentLoadingDefault();
      let dataparams:any = {
        fName: this.registrationfields.fName.trim(),
        lName: this.registrationfields.lName.trim(),
        number: this.registrationfields.number.trim(),
        email: this.registrationfields.email.trim(),
        password: this.registrationfields.password.trim(),
        confirmPassword:this.registrationfields.confirmPassword.trim(),
      };
      this.userService.registrationService('user',dataparams)
      .then(data => {
        let dataobj:any = JSON.parse(data.data);
        console.log(dataobj)
        if(dataobj){
          this.cs.presentLoadingClose();
          this.cs.presentToast(dataobj.message);
          this.navCtrl.setRoot('UserOtpVerificationPage', {
            email:this.registrationfields.email.trim()
          });
        }
      })
      .catch(error => {
        console.log('error',error);
        let dataobj:any = JSON.parse(error.error);
        this.cs.presentToast(dataobj.message);
        this.cs.presentLoadingClose();
      });
     
      // this.userService.checkSession(params).then(data => {
        
      //   if(data){
      //     console.log(data)
      //   }
      // })
     
    }
  }

}
