import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CustomService } from './../../app/customservice';
import { UserServiceProvider } from '../../providers/user-service/user-service';

@IonicPage()
@Component({
  selector: 'page-user-update-profile',
  templateUrl: 'user-update-profile.html',
})
export class UserUpdateProfilePage {
  public userData: any = {
    fName: '',
    lName: '',
    email: '',
    number: '',
    apartmentName: '',
    apartmentAddress: '',
    apartmentNo: ''
  }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public cs: CustomService,
    public userService: UserServiceProvider
  ) {
    let paramdata = this.navParams.get("paramdata");
    if (paramdata) {
      this.userData.fName = paramdata.personalDetails.fName;
      this.userData.lName = paramdata.personalDetails.lName;
      this.userData.email = paramdata.personalDetails.email;
      this.userData.number = paramdata.personalDetails.number;
      this.userData.id = paramdata.personalDetails.id;
    }else{
      let userId = localStorage.getItem('userId');
      if(userId){
        this.cs.presentLoadingDefault();
        this.userService.userService('accountDetails', {userId:userId})
        .then(data => {
          console.log(data)
          let dataobj: any = JSON.parse(data.data);
          if (dataobj && dataobj.personalDetails != null || dataobj.apartmentDetails !=null) {
            this.userData.fName = dataobj.personalDetails.fName;
            this.userData.lName = dataobj.personalDetails.lName;
            this.userData.email = dataobj.personalDetails.email;
            this.userData.number = dataobj.personalDetails.number;
            this.userData.id = dataobj.personalDetails.id;
            this.userData.apartmentName = dataobj.apartmentDetails.apartmentName;
            this.userData.apartmentAddress = dataobj.apartmentDetails.apartmentAddress;
            this.userData.apartmentNo= dataobj.apartmentDetails.apartmentNumber;
            this.cs.presentLoadingClose();
          }
        })
        .catch(error => {
          console.log('error', error);
          let dataobj: any = JSON.parse(error.error);
          this.cs.presentToast(dataobj.message);
          this.cs.presentLoadingClose();
        });
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserUpdateProfilePage');
  }
  update() {
    if (!this.userData.apartmentAddress.trim()) {
      this.cs.presentToast('Enter apartment Address');
    } else if (!this.userData.apartmentName.trim()) {
      this.cs.presentToast('Enter apartment Name');
    } else if (!this.userData.apartmentNo.trim()) {
      this.cs.presentToast('Enter apartment id');
    } else {

      this.cs.presentLoadingDefault();
      let dataparams: any = {
        userId: this.userData.id,
        apartmentAddress: this.userData.apartmentAddress,
        apartmentName: this.userData.apartmentName,
        apartmentNumber: this.userData.apartmentNo
      }
      console.log(dataparams)
      this.userService.userService('updateDetails', dataparams)
        .then(data => {
          console.log(data)
          let dataobj: any = JSON.parse(data.data);
          console.log(dataobj);
          this.cs.presentLoadingClose();
          this.cs.presentToast(dataobj.message);
          localStorage.getItem('userId')?null:this.navCtrl.setRoot('UserLoginPage');
        })
        .catch(error => {
          console.log('error', error);
          let dataobj: any = JSON.parse(error.error);
          this.cs.presentToast(dataobj.message);
          this.cs.presentLoadingClose();
        });
    }
  }
}
