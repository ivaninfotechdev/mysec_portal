import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CustomService } from './../../app/customservice';
import { UserServiceProvider } from '../../providers/user-service/user-service';

import { CallNumber } from '@ionic-native/call-number';

@IonicPage()
@Component({
  selector: 'page-manage-visitor',
  templateUrl: 'manage-visitor.html',
})
export class ManageVisitorPage {
  public requstedName : any="";
  public visitorList:any=[];
  public categoryList = [
    { label: 'VISITOR', 'value': 'v' },
    { label: 'MAID', 'value': 'M' },
    { label: 'RELATIVE', 'value': 'R' },
    { label: 'PERSONAL', 'value': 'P' },
    { label: 'DELIVERY', 'value': 'D' }
  ]
  jsonData : any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public cs: CustomService,
    public userService: UserServiceProvider,
    private callNumber: CallNumber
    
    ) {
      
  }

  onInput(ev: any){
    const val = ev.target.value;
    if (val && val.trim() != ''){
        this.visitorList = this.visitorList.filter((item) => {
          return (item.fName.toLowerCase().indexOf(val.toLowerCase()) > -1);   
        })
    }else{
      this.getVisitorList();
    }
  }

  ionViewWillEnter() {
    console.log('ionViewDidLoad ManageVisitorPage');
    this.getVisitorList()
  }

  call(number){
    this.callNumber.callNumber(number, true)
  .then(res => console.log('Launched dialer!', res))
  .catch(err => console.log('Error launching dialer', err));
  }
  
  addVisitor(){
    this.navCtrl.push('AddNewVisitorPage')
  }

  getVisitorList(){
    this.visitorList = [];
    this.cs.presentLoadingDefault();
    let UserId = localStorage.getItem('userId');
    let dataparams = {
      appartmentOwnerId:UserId
    }
    this.userService.visitorUserService('visitors', dataparams)
    .then(data => {
      console.log(data)
      this.cs.presentLoadingClose();
      let dataobj: any = JSON.parse(data.data);
      //console.log(dataobj)
      if (dataobj) {
        this.visitorList = dataobj;
        for(let j=0; j<this.visitorList.length; j++){
          let category = this.visitorList[j].category;
         
          for(let i=0; i<this.categoryList.length; i++){
            if((this.categoryList[i].value).toLowerCase() == category.toLowerCase()){
              console.log(category)
              this.visitorList[j].viewCat = this.categoryList[i].label;
              break;
            }
          }
        }
        console.log(this.visitorList)
        
        this.cs.presentToast(dataobj.message);
      }
    }).catch((err) => {
      this.cs.presentLoadingClose();
      let data = err;
      console.log(err)
      if (data.status == 400) {
        let dataobj: any = JSON.parse(data.error);
        this.cs.presentToast(dataobj.message);
        console.log(dataobj)
      } else {
        let dataobj: any = JSON.parse(data.error);
        this.cs.presentToast(dataobj.message);
      }
    })
  }

}
