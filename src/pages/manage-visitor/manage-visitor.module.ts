import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ManageVisitorPage } from './manage-visitor';

@NgModule({
  declarations: [
    ManageVisitorPage,
  ],
  imports: [
    IonicPageModule.forChild(ManageVisitorPage),
  ],
})
export class ManageVisitorPageModule {}
