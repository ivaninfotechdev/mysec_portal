import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserOtpVerificationPage } from './user-otp-verification';

@NgModule({
  declarations: [
    UserOtpVerificationPage,
  ],
  imports: [
    IonicPageModule.forChild(UserOtpVerificationPage),
  ],
})
export class UserOtpVerificationPageModule {}
