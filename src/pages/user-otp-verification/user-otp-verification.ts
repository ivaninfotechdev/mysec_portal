import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CustomService } from './../../app/customservice';
import {UserServiceProvider} from '../../providers/user-service/user-service';

@IonicPage()
@Component({
  selector: 'page-user-otp-verification',
  templateUrl: 'user-otp-verification.html',
})
export class UserOtpVerificationPage {
  email:any;
	public otp:any = {
		input1:"", input2:"", input3:"", input4:""
	};
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public cs: CustomService,
    public userService:UserServiceProvider
  ) {
    this.email = this.navParams.get("email");
  }

  moveFocus(next:any, event:any, prev:any){
    if(event.key == 'Backspace'){
      prev.setFocus();
    }else{
      next.setFocus();
    }
  }

  submit(){
    console.log(this.otp.input1.trim()+this.otp.input2.trim()+this.otp.input3.trim()+this.otp.input4.trim())
    if(!this.otp.input1){
      this.cs.presentToast("Enter OTP code")
    }else if(!this.otp.input2){
      this.cs.presentToast("Enter OTP code")
    }else if(!this.otp.input3){
      this.cs.presentToast("Enter OTP code")
    }else if(!this.otp.input4){
      this.cs.presentToast("Enter OTP code")
    }else{
      this.cs.presentLoadingDefault();
      let dataparams = {
        otp:this.otp.input1.trim()+this.otp.input2.trim()+this.otp.input3.trim()+this.otp.input4.trim(),
        email:this.email
      }
      this.userService.verifyOTPService('otpEmail',dataparams)
      .then(data => {
        let dataobj:any = JSON.parse(data.data);
        console.log(dataobj)
        if(dataobj){
          console.log(dataobj)
          this.cs.presentLoadingClose();
          this.cs.presentToast(dataobj.otpVerficationMsg);
          this.navCtrl.push('UserUpdateProfilePage', {paramdata:dataobj});
        }
      })
      .catch(error => {
        console.log('error',error);
        let dataobj:any = JSON.parse(error.error);
        this.cs.presentToast(dataobj.message);
        this.cs.presentLoadingClose();
      });
    }
    
  }

  resendOtp(){
    
  }

}
