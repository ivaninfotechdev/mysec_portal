import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ManagePreferencePage } from './manage-preference';

@NgModule({
  declarations: [
    ManagePreferencePage,
  ],
  imports: [
    IonicPageModule.forChild(ManagePreferencePage),
  ],
})
export class ManagePreferencePageModule {}
